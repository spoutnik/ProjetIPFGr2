let m = Binop("f", Var("x"), Const("a"));;
let t = Binop("f", Binop("f", Const("a"), Const("b")), Const("a"));;
let t1 = Binop("f", Binop("f", Const("a"), Const("b")), Const("b"));;

variables m;;
variables t;;

isInstance m t;;
getSubstitutions m t;;
(*getSubstitutions m t1;;
isBienForme sys [];;

toString t1;;*)

let rec printsys sys = match sys with
    | [] -> ()
    | (tg, td)::reste -> Printf.printf "%s -> %s\n" (toString tg) (toString td); printsys reste;;

let sys = [
    (Binop("plus", Var("x"),        Const("zero")), Var("x"));
    (Binop("plus", Const("zero"),   Var("x")),      Var("x"));
    (Binop("plus", Unop("succ",     Var("x")),      Var("y")), 
     Binop("plus", Var("x"),        Unop("succ", Var("y"))))
    ];; 

printsys sys;;

let ti = Binop("plus", Const("zero"), Binop("plus", Unop("succ", Const("zero")), Unop("succ", Const("zero"))));;

Printf.printf "%s\n" (toString ti);;

conforme (Binop("plus", Var("x"), Const("zero"))) [("plus", 2); ("zero", 0)];;

forceReglesVerbose ti sys;;
