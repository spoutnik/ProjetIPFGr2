# Projet IPF - Christian Morello

## Présentation du travail

### Introduction

On cherche à implémenter une réécriture automatique de termes logiques. Ici les termes sont composés:
- De variables;
- De constantes, symboles d'arité `0`;
- De fonctions prenant un terme comme argument, symboles d'arité `1`;
- De fonctions prenant deux termes comme arguments, symboles d'arité `2`.

On les définit de la façon suivante:
```ocaml
type terme =
    | Var of string 
    | Const of string
    | Unop of string * terme 
    | Binop of string * terme * terme;;
```
### Implémentation

On vérifie tout d'abord qu'un terme est bien formé pour une signature donnée, c'est à dire que tout symbole du terme appartient à cette signature, ainsi que d'arité spécifiée par cette signature. Pour réaliser cela, la fonction `conforme` va parcourir le terme et appeler `isIn` pour chaque symbole rencontré, qui est en fait un appel à `List.mem`, retournant `true` si tous les appels à `isIn` renvoient eux aussi `true`.  

Il faut ensuite déterminer si un terme bien formé est un motif, c'est à dire si chaque variable que contient le terme n'apparaît qu'une seule et unique fois dans ce dernier. Pour cela, j'ai créé la fonction `variables`, qui va dans un premier temps faire la liste de toutes les variables d'un terme. Ensuite, une fonction `occurences` va compter dans le terme le nombre d'occurences de cette variable: une dernière fonction `isMotif` va utiliser ces deux fonctions, en plus de `conforme`, pour déterminer si le terme est un motif.  
Le fait d'utiliser `variables`, puis de lancer `occurences` peut paraître peu optimisé, mais il se révèle être un bon compromis car `variables` pourra ensuite être réutilisé pour les questions suivantes.

L'étape suivante consiste à calculer les substitutions permettant de transformer un motif en un terme filtré par ce dernier. `isInstance` et `substitutions` permettent de générer cette liste de substitutions, et ont un fonctionnement similaire: elles von parcourir "l'arbre" que forme le terme et s'intéresser aux feuilles. `isInstance` renvoie `false` si elle trouve une différence dans toute feuille qui n'est pas une variable dans le motif; `substitutions` va elle renvoyer le couple `(string * terme)` qu'elle trouve à toute différence de feuille.  
Pour extraire les substitutions, `getSubstitutions` va alors lancer le test `isInstance` pour vérifier que le terme est une instance du motif, puis `substitutions` si le premier appel à réussi, car `substitutions` présuppose que le motif filtre le terme.

La dernière étape de la préparation est de vérifier la légitimité d'un système de réécriture: une liste de règles indiquant la possibilité de réécrire un motif en un autre terme. La difficulté est de vérifier que la règle n'introduit pas de nouvelles variables: les autres critères correspondent aux fonctions réalisées précedemment.  
On réutilise ici `variables`, en testant si la liste résultant du terme `td` est inclus dans celle résultant du terme `tg`, en utilisant `inclus`.

Pour appliquer les règles d'un système à un terme jusqu'à ce que ce dernier ne change plus, il a d'abord fallu implémenter `equals`, qui teste l'égalité de deux termes en profondeur.  
Ensuite, une fonction `appliqueRegles` va itérer à travers le système et appliquer chaque règle de substitution qu'elle va y trouver, en appelant `substituteAll`, une version récursive de `substitute`, qui va transformer le terme suivant la règle qui lui est passée.
Enfin, `appliqueRegles` est appelée par `forceRegles` tant que le terme n'est pas identique à lui même après être passé dans `appliqueRegles`, en testant l'égalité avec `equals`.

Pour afficher les règles appliquées, j'ai créé la fonction `toString` pour transformer un terme en `string`: à chaque appel de `substituteAll`, une fonction `speak` va être lancée pour afficher toutes les informations nécessaires à l'aide de `toString` avant la modification du terme.

Pour finir, la fonction `demo` va lancer `equals` sur deux termes, après application de `forceRegles` sur chacun d'entre eux, pour montrer que deux termes sont égaux.

### Conclusion

L'algorithme naïf mit en place, malgré les boucles superflues, agit de façon remarquable. L'objectif a été atteint: l'algorithme est capable de réécrire des termes, une fois donné une règle de réécriture.

## Annexe

### Interfaces

```ocaml
Question 1

(** isIn
 * @param symbole: Le symbole a tester
 * @param arite: L'arité de ce symbole
 * @param signature: La signature où chercher le couple
 * @return: Booléen indiquant si le couple (symbole, arité) est dans la signature
 *)

(** conforme
 * @param terme: La test sur lequel performer le test de conformité
 * @param signature: La signature à utiliser
 * @return: Booléen indiquant si le terme est conforme à `signature`
 *)

Question 2

(** variables
 * @param terme: Le terme duquel extraire les variables
 * @return: Une liste des variables contenues dans terme
 *)

(** occurences
 * @param var: La variable à chercher
 * @param terme: Le terme où chercher cette variable
 * @return: Le nombre d'occurences de `var` dans `terme`
 *)

(** isMotif
 * @param terme: Le terme à tester
 * @param signature: La signature à utiliser
 * @return: Booléen indiquant si le terme est un motif
 *)

Question 3

(** isInstance
 * @param motif: Le motif à utiliser
 * @param terme: Le terme à tester
 * @return: Booléen indiquant si `terme` est une instance de `motif`
 *)

(** substitutions
 * @param motif: Le motif à utiliser
 * @param terme: Le terme où extraire les substitutions
 * @pre: Le motif doit filtrer le terme
 * @return: Une liste de couples `(string * terme)`, les substitutions à effectuer
 *)

(** getSubstitutions
 * @param motif: Le motif à utiliser
 * @param terme: Le terme où extraire les substitutions
 * @return: Une liste de couples `(string * terme)`, les substitutions à effectuer
 *)

Question 4

(** inclus
 * @param listeComplete: La liste de référence
 * @param listeATester: La liste à tester
 * @return: Booléen indiquant si `listeATester` est incluse dans `listeComplete`
 *)

(** sameVars
 * @param tg: Le terme d'origine
 * @param td: Le terme à substituer
 * @return: Booléen indiquant si `td` n'introduit PAS de nouvelles variables avec `true`
 *)

(** isBienForme
 * @param systeme: Le systeme à tester
 * @param signature: la signature à utiliser
 * @return: Booléen indiquant si le système est bien formé
 *)

Question 5

(** substitute
 * @param terme: Le terme à modifier
 * @param sub: La substitution à appliquer
 * @return: Le terme, avec la substitution appliquée
 *)

(** substituteAll
 * @param terme: Le terme à modifier
 * @param subList: La liste de substitutions à appliquer
 * @return: Le terme, avec les substitutions appliquées
 *)

(** appliqueRegles
 * @param terme: Le terme à modifier
 * @param systeme: Le système à appliquer
 * @return: Le terme, avec le système ayant été appliqué une fois
 *)

(** equals
 * @param t1: Premier terme à tester
 * @param t2: Second terme à tester
 * @return: t1 '==' t2
 *)

(** forceRegles
 * @param terme: Le terme à modifier
 * @param systeme: Le système à appliquer
 * @return: Le terme, avec le système ayant été appliqué jusqu'à ce qu'il
n'affecte plus `terme`
 *)

Question 6

(** toString
 * @param terme: Le terme à considérer
 * @return: Une `string` décrivant le terme
 *)

(** speak
 * @param tg: La partie de gauche de la règle à appliquer
 * @param td: La partie de droite de la règle à appliquer
 * @param terme: Le terme à modifier
 * @param index: Le numéro de la règle à appliquer
 * @return: Le terme, avec la règle ayant été appliquée et affichée à l'écran
 *)

(** appliqueReglesVerbose
 * @param terme: Le terme à modifier
 * @param systeme: Le système à appliquer
 * @return: Le terme, avec le système ayant été appliqué une fois,
et les informations affichées à l'écran
 *)

(** forceReglesVerbose
 * @param terme: Le terme à modifier
 * @param systeme: Le système à appliquer
 * @return: Le terme, avec le système ayant été appliqué jusqu'à ce qu'il
n'affecte plus terme, et les informations affichées à l'écran
 *)

Question 7

(** demo
 * @param t1: Le premier terme
 * @param t2: Le second terme
 * @param sys: Le système à utiliser
 * @return: Booleen indiquant si les deux termes sont égaux après
application du système `sys`
 *)
```
