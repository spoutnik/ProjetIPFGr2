type terme = 
    | Var of string
    | Const of string
    | Unop of string*terme 
    | Binop of string*terme*terme;;

(** isIn
 * @param symbole: Le symbole a tester
 * @param arite: L'arité de ce symbole
 * @param signature: La signature où chercher le couple
 * @return: Booléen indiquant si le couple (symbole, arité) est dans la signature
 *)
let rec isIn symbole arite signature = 
    List.mem (symbole, arite) signature;;

(** conforme
 * @param terme: La test sur lequel performer le test de conformité
 * @param signature: La signature à utiliser
 * @return: Booléen indiquant si le terme est conforme à `signature`
 *)
let rec conforme terme signature = match terme with
    | Var v -> true
    | Const c -> (isIn c 0 signature)
    | Unop(s, t) -> (isIn s 1 signature) && (conforme t signature)
    | Binop(s, t1, t2) -> (isIn s 2 signature) && (conforme t1 signature) && (conforme t2 signature);;


(** variables
 * @param terme: Le terme duquel extraire les variables
 * @return: Une liste des variables contenues dans terme
 *)
let rec variables terme = match terme with
    | Var v -> [v]
    | Const c -> []
    | Unop(s, t) -> variables t
    | Binop(s, t1, t2) -> (variables t1) @ (variables t2);;


(** occurences
 * @param var: La variable à chercher
 * @param terme: Le terme où chercher cette variable
 * @return: Le nombre d'occurences de `var` dans `terme`
 *)
let rec occurences var terme = match terme with
    | Var v -> if (v = var) then 1 else 0
    | Const c -> 0
    | Unop(s, t) -> occurences var t
    | Binop(s, t1, t2) -> (occurences var t1) + (occurences var t2);;


(** isMotif
 * @param terme: Le terme à tester
 * @param signature: La signature à utiliser
 * @return: Booléen indiquant si le terme est un motif
 *)
let isMotif terme signature =
    let rec aux terme vars = match vars with
        | [] -> true
        | v::reste -> (occurences v terme = 1) && (aux terme reste)
    in
    (conforme terme signature) && (aux terme (variables terme));;


(** isInstance
 * @param motif: Le motif à utiliser
 * @param terme: Le terme à tester
 * @return: Booléen indiquant si `terme` est une instance de `motif`
 *)
let rec isInstance motif terme = match (motif, terme) with
    | (Var s1, Var s2) -> false
    | (Var s1, _) -> true
    | (Const c1, Const c2) -> c1 = c2
    | (Const c1, _) -> false
    | (Unop(s1, t1), Unop(s2, t2)) -> (s1 = s2) && (isInstance t1 t2)
    | (Unop(s1, t1), _) -> false
    | (Binop(s1, t11, t12), Binop(s2, t21, t22)) -> (s1 = s2) && (isInstance t11 t21) && (isInstance t12 t22)
    | (Binop(s1, t11, t12), _) -> false;;


(** substitutions
 * @param motif: Le motif à utiliser
 * @param terme: Le terme où extraire les substitutions
 * @pre: Le motif doit filtrer le terme
 * @return: Une liste de couples `(string * terme)`, les substitutions à effectuer
 *)
let rec substitutions motif terme = match (motif, terme) with
    | (Var s1, t) -> [(s1, t)]
    | (Const c1, Const c2) -> []
    | (Const c1, _) -> []
    | (Unop(s1, t1), Unop(s2, t2)) -> substitutions t1 t2
    | (Unop(s1, t1), _) -> []
    | (Binop(s1, t11, t12), Binop(s2, t21, t22)) -> (substitutions t11 t21) @ (substitutions t12 t22)
    | (Binop(s1, t11, t12), _) -> [];;


(** getSubstitutions
 * @param motif: Le motif à utiliser
 * @param terme: Le terme où extraire les substitutions
 * @return: Une liste de couples `(string * terme)`, les substitutions à effectuer
 *)
let getSubstitutions motif terme =
    if (isInstance motif terme) then substitutions motif terme else failwith "Le motif ne filtre pas le terme !";;


(** inclus
 * @param listeComplete: La liste de référence
 * @param listeATester: La liste à tester
 * @return: Booléen indiquant si `listeATester` est incluse dans `listeComplete`
 *)
let rec inclus listeComplete listeATester = match listeATester with
    | [] -> true
    | h::q -> (List.mem h listeComplete) && (inclus listeComplete q);;


(** sameVars
 * @param tg: Le terme d'origine
 * @param td: Le terme à substituer
 * @return: Booléen indiquant si `td` n'introduit PAS de nouvelles variables avec `true`
 *)
let sameVars tg td =
    inclus (variables tg) (variables td);;


(** isBienForme
 * @param systeme: Le systeme à tester
 * @param signature: la signature à utiliser
 * @return: Booléen indiquant si le système est bien formé
 *)
let isBienForme systeme signature =
    let rec aux systeme signature = match systeme with
        | [] -> true
        | (tg, td)::reste -> (conforme td signature) && (isMotif td signature) && (sameVars tg td) && (aux reste signature)
    in

    aux systeme signature;;


(** substitute
 * @param terme: Le terme à modifier
 * @param sub: La substitution à appliquer
 * @return: Le terme, avec la substitution appliquée
 *)
let rec substitute terme sub = match terme with
    | Var v -> let (nom, valeur) = sub in if (nom = v) then valeur else Var(v)
    | Unop(s, t) -> Unop(s, (substitute t sub))
    | Binop(s, t1, t2) -> Binop(s, (substitute t1 sub), (substitute t2 sub))
    | x -> x;;


(** substituteAll
 * @param terme: Le terme à modifier
 * @param subList: La liste de substitutions à appliquer
 * @return: Le terme, avec les substitutions appliquées
 *)
let rec substituteAll terme subList = match subList with
    | [] -> terme
    | h::q -> substituteAll (substitute terme h) q;;


(** appliqueRegles
 * @param terme: Le terme à modifier
 * @param systeme: Le système à appliquer
 * @return: Le terme, avec le système ayant été appliqué une fois
 *)
let rec appliqueRegles terme systeme = match systeme with
    | [] -> terme
    | (tg, td)::reste -> appliqueRegles (substituteAll td (getSubstitutions tg terme)) reste;;


(** equals
 * @param t1: Premier terme à tester
 * @param t2: Second terme à tester
 * @return: t1 '==' t2
 *)
let rec equals t1 t2 = match (t1, t2) with
    | (Var s1, Var s2) -> s1 = s2
    | (Var s1, _) -> true
    | (Const c1, Const c2) -> c1 = c2
    | (Const c1, _) -> false
    | (Unop(s1, t1), Unop(s2, t2)) -> (s1 = s2) && (equals t1 t2)
    | (Unop(s1, t1), _) -> false
    | (Binop(s1, t11, t12), Binop(s2, t21, t22)) -> (s1 = s2) && (equals t11 t21) && (equals t12 t22)
    | (Binop(s1, t11, t12), _) -> false;;


(** forceRegles
 * @param terme: Le terme à modifier
 * @param systeme: Le système à appliquer
 * @return: Le terme, avec le système ayant été appliqué jusqu'à ce qu'il n'affecte plus terme
 *)
let rec forceRegles terme systeme =
    let result = appliqueRegles terme systeme in

    if (equals result terme) then
        result
    else
        forceRegles result systeme;;

(*--------------------------------------------*)


(** toString
 * @param terme: Le terme à considérer
 * @return: Une `string` décrivant le terme
 *)
let rec toString terme = match terme with
    | Var v -> v
    | Const c -> c
    | Unop(s, t) -> s ^ "(" ^ (toString t) ^ ")"
    | Binop(s, t1, t2) -> s ^ "(" ^ (toString t1) ^ ", " ^ (toString t2) ^ ")";;


(** speak
 * @param tg: La partie de gauche de la règle à appliquer
 * @param td: La partie de droite de la règle à appliquer
 * @param terme: Le terme à modifier
 * @param index: Le numéro de la règle à appliquer
 * @return: Le terme, avec la règle ayant été appliquée et affichée à l'écran
 *)
let speak tg td terme index =
    Printf.printf "Par r%d: " index;
    let result = (substituteAll td (substitutions tg terme)) in
    Printf.printf "%s\n" (toString result);
    result;;


(** appliqueReglesVerbose
 * @param terme: Le terme à modifier
 * @param systeme: Le système à appliquer
 * @return: Le terme, avec le système ayant été appliqué une fois, et les informations affichées à l'écran
 *)
let rec appliqueReglesVerbose terme systeme index = match systeme with
    | [] -> terme
    | (tg, td)::reste -> if (isInstance tg terme) then
        appliqueReglesVerbose (speak tg td terme index) reste (index+1)
    else
        appliqueReglesVerbose terme reste (index+1);;


(** forceReglesVerbose
 * @param terme: Le terme à modifier
 * @param systeme: Le système à appliquer
 * @return: Le terme, avec le système ayant été appliqué jusqu'à ce qu'il n'affecte plus terme, et les informations affichées à l'écran
 *)
let rec forceReglesVerbose terme systeme =
    let result = appliqueReglesVerbose terme systeme 1 in

    if (equals result terme) then
        result
    else
        forceReglesVerbose result systeme;;

(*--------------------------------------------*)


(** demo
 * @param t1: Le premier terme
 * @param t2: Le second terme
 * @param sys: Le système à utiliser
 * @return: Booleen indiquant si les deux termes sont égaux après application du système `sys`
 *)
let demo t1 t2 sys = 
    equals (forceRegles t1 sys) (forceRegles t2 sys);;
